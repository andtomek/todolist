# Todolist

Project uses React, Material UI, ITCSS. MongoDB for database, Mongoose object modeling. Express for server-side e.g. API.

## How to start

Make sure you have MongoDB installed on your machine.

On one command instance, let's run MongoDB
```sh
$ cd todolist
$ mongod
```

On the next command instance, we will install all npm dependencies and then start up our server API:
```sh
$ cd todolist
$ npm install
$ npm run dev-server
```

And on another one, we will start webpack with our front-end stuff:
```sh
$ cd todolist
$ npm start
```

## Notes

Tested on Chrome. API returns data as specified in documentation, but I added from myself a "todos_completed" field in Todolist model.
