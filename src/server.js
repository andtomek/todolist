const express = require('express'),
      app = express(),
      port = process.env.PORT || 3000,
      mongoose = require('mongoose'),
      Todolist = require('./api/models/todoListModel'), 
      Todos = require('./api/models/todosModel')
      bodyParser = require('body-parser');
  

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/Todolistdb'); 

app.use( bodyParser.urlencoded({ extended: true }) );
app.use( bodyParser.json() );

//loading routes
const todoListRoutes = require('./api/routes/todoListRoutes'); 
todoListRoutes(app); 

const todosRoutes = require('./api/routes/todosRoutes');
todosRoutes(app);


app.listen(port);

console.log('RESTful API server available on: ' + port);