'use strict';
module.exports = app => {
  const todos = require('../controllers/todosController');
  const cors = require('cors');

  app.use(cors());

  // todos Routes
  app.route('/todos')
    .get(todos.listAllTodos)
    .post(todos.createTodo);

  app.route('/todos/:todoId')
    .get(todos.listTodo)
    .put(todos.updateTodo)
    .delete(todos.deleteTodo);
};