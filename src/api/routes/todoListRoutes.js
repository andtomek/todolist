'use strict';
module.exports = app => {
  const todoList = require('../controllers/todoListController');
  const cors = require('cors');

  app.use(cors());

  // todoList Routes
  app.route('/todolists')
    .get(todoList.listAllTodolists)
    .post(todoList.createTodolist)


  app.route('/todolists/:todolistId')
    .get(todoList.listTodos)
    .put(todoList.updateTodolist)
    .delete(todoList.deleteTodolist);
};