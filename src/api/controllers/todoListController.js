'use strict';


const mongoose = require('mongoose');
const Todolists = mongoose.model('Todolists');
const Todos = mongoose.model('Todos');

exports.listAllTodolists = (req, res) => {
  Todolists.find({}, (err, task) => {
    if (err)
      res.send(err);
    res.json(task);
  });
};

exports.createTodolist = (req, res) => {
  const newTodolists = new Todolists(req.body);
  newTodolists.save( (err, todolist) => {
    if (err)
      res.send(err);
    res.json(todolist);
  });
};

exports.listTodos = (req, res) => {
  Todos.find( { 'todo_list': req.params.todolistId}, (err, todolist) => {
    if (err)
      res.send(err);
    res.json(todolist);
  });
};

exports.updateTodolist = (req, res) => {
  Todolists.findOneAndUpdate({ _id: req.params.todolistId}, req.body, {new: true}, (err, todolist) => {
    if (err)
      res.send(err);
    res.json(todolist);
  });
};

exports.deleteTodolist = (req, res) => {
  Todolists.remove({ _id: req.params.todolistId }, (err, todolist) => {
    if (err)
      res.send(err);
  })
  .then( () => {
    //removing all todos of this todolist
    Todos.remove( { 'todo_list': req.params.todolistId}, (err, todolist) => {
      if (err)
        res.send(err);
      res.json(true); 
    });
  })
};