'use strict';

const mongoose = require('mongoose');
const Todos = mongoose.model('Todos');
const Todolists = mongoose.model('Todolists');

exports.listAllTodos = (req, res) => {
  Todos.find({}, (err, task) => {
    if (err)
      res.send(err);
    res.json(task);
  });
};


exports.createTodo = (req, res) => {
  const newTodos = new Todos(req.body);
  newTodos
    .save( (err, task) => {
      if (err)
        res.send(err);
      return task;
    })
    .then( (task) => {
      //increasing `todos_count` of parent todolist
      Todolists.findOneAndUpdate({ _id: task.todo_list}, { $inc: { todos_count: 1 }}, {new: true}, (err, todolist) => {
        if (err)
          res.send(err);
        
        res.json(task);
      });
    });
};


exports.listTodo = (req, res) => {
  Todos.findById(req.params.todoId, (err, task) => {
    if (err)
      res.send(err);
    res.json(task);
  });
};


exports.updateTodo = (req, res) => {

  Todos
    .findOneAndUpdate({ _id: req.params.todoId}, req.body, {new: true}, (err, task) => {
      if (err)
        res.send(err);
    
      //increasing or decreasing `todos_completed` of parent todolist
      let change = 0;
      if ( req.body.hasOwnProperty("is_complete") ) {
        change = req.body.is_complete ? 1 : -1;
      } 

      Todolists.findOneAndUpdate({ _id: task.todo_list}, { $inc: { todos_completed: change }}, (err, todolist) => {
        if (err)
          res.send(err);
        
        res.json(task);
      });

    });

};


exports.deleteTodo = (req, res) => {
  
  Todos
    .findByIdAndRemove({ _id: req.params.todoId }, (err, task) => {
      if (err)
        res.send(err);
      
      //decreasing `todos_count` and/or `todos_completed` of parent todolist
      const change = task.is_complete ? -1 : 0;
      Todolists.findOneAndUpdate({ _id: task.todo_list}, { $inc: { todos_count: -1, todos_completed: change }}, (err, todolist) => {
        if (err)
          res.send(err);
        
        res.json(true);
      });
    });

};