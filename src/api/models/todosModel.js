'use strict';
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const TodosSchema = new Schema({
  name: String,
  is_complete: {
    type: Boolean,
    default: false
  },
  todo_list: String
});

TodosSchema.set('toJSON', {
  transform: (doc, ret, options) => {
    ret.id = ret._id;
    delete ret._id;
    delete ret.__v;
  }
}); 

module.exports = mongoose.model('Todos', TodosSchema);