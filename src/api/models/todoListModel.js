'use strict';
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const TodolistsSchema = new Schema({
  name: String,
  todos_count: {
    type: Number,
    default: 0
  },
  todos_completed: {
    type: Number,
    default: 0
  }
});

TodolistsSchema.set('toJSON', {
  transform: (doc, ret, options) => {
    ret.id = ret._id;
    delete ret._id;
    delete ret.__v;
  }
}); 

module.exports = mongoose.model('Todolists', TodolistsSchema);