import React from 'react'
import { render } from 'react-dom'

import App from './containers/App'
import './layout/main'

render(<App />, document.querySelector('#app'));
