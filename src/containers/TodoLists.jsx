import React from 'react'
import ReactDOM from 'react-dom'

import ApiFetchedList from '../components/ApiFetchedList';
import TodolistItem from '../components/TodolistItem';

export default class TodoLists extends React.Component {

    map(item) {
        item.is_complete = (item.todos_count === item.todos_completed) && item.todos_count > 0;
        return item;
    }

    render() {

        const apiUrls = {
            getAll: '/api/todolists/',
            delete: '/api/todolists/',
            create: '/api/todolists/',
            update: '/api/todolists/'
        }

        const errorMsgs = {
            getAllItems: "Error occured when getting the list of todolists. Please try again.",
            deleteItem: "Error occured when deleting the todolist. Please try again.",
            addItem: "Error occured when creating a new todolist. Please try again.",
            updateItem: "Error occured when updating the todolist. Please try again."
        }

        return (
          <div className="o-wrapper">
            <main className="o-layout">
              <div className="o-layout__item">
                <ApiFetchedList
                  apiUrls={apiUrls}
                  listName={{parent:"todolists", child:"todolist"}}
                  errorMsgs={errorMsgs}
                  childComponent={TodolistItem}
                  map={this.map.bind(this)}
                  />
              </div>
            </main>
          </div>
        );
    }    

};