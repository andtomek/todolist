import React from 'react'
import { BrowserRouter, Switch, Route } from "react-router-dom";

import TodoLists from './TodoLists'
import TodoList from './TodoList'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'

export default class App extends React.Component {

  render() {
      return (
        <MuiThemeProvider>
          <BrowserRouter>
            <Switch>
              <Route exact path="/" component={TodoLists}/>
              <Route exact path="/todolist" component={TodoList} />
            </Switch>
          </BrowserRouter>
        </MuiThemeProvider>
      )
  }
}
