import React from 'react'
import ReactDOM from 'react-dom'
import { Redirect } from 'react-router'

import FlatButton from 'material-ui/FlatButton';
import FontIcon from 'material-ui/FontIcon';

import ApiFetchedList from '../components/ApiFetchedList';
import TodoListHeader from '../components/TodoListHeader';
import ListItem from '../components/ListItem';
import Navigate from '../components/Navigate';

export default class TodoList extends React.Component {

    render() {
        const {state} = this.props.location;

        if ( !state ) { // `/todolist` path is only for easy in-browser nagivation, redirect on direct access 
          return <Redirect to="/" push={true} />;
        }

        const {id, name} = state;

        const apiUrls = {
            getAll: `/api/todolists/${id}`,
            delete: '/api/todos/',
            create: '/api/todos/',
            update: '/api/todos/',
            get: '/api/todos/'
        }

        const errorMsgs = {
            getAllItems: "Error occured when getting the list of todos. Please try again.",
            deleteItem: "Error occured when deleting the todo. Please try again.",
            addItem: "Error occured when creating a new todo. Please try again.",
            updateItem: "Error occured when updating the todo. Please try again."
        }

        return (
            <div className="o-wrapper">
              <main className="o-layout">
                <div className="o-layout__item">
                  <Navigate to="/">
                    <FlatButton 
                      icon={<FontIcon className="material-icons">keyboard_backspace</FontIcon>} 
                      label="Back" 
                      fullWidth={true}
                      />
                  </Navigate>
                  <TodoListHeader id={id} name={name} />
                  <ApiFetchedList
                    todolistId={id} 
                    listName={{parent: "todolist" + id}}
                    apiUrls={apiUrls}
                    errorMsgs={errorMsgs}
                    childComponent={ListItem}
                    />
                </div>
              </main>
            </div>
        );
    }    

};