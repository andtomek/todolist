import React from 'react'
import ReactDOM from 'react-dom'

import { Draggable } from 'react-beautiful-dnd';

import TextField from 'material-ui/TextField';
import IconButton from 'material-ui/IconButton';
import Checkbox from 'material-ui/Checkbox';
import FontIcon from 'material-ui/FontIcon';

export default class ListItem extends React.Component {

    constructor() {
        super();

        this.state = {
            inFocus: false,
            underlineShow: false,
            changedTextInput: false
        }

        this.delete = this.delete.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleFocus = this.handleFocus.bind(this);
        this.handleBlur = this.handleBlur.bind(this);
        this.handleMouseOver = this.handleMouseOver.bind(this);
        this.handleMouseLeave = this.handleMouseLeave.bind(this);
        this.handleKeyPress = this.handleKeyPress.bind(this);
        this.handleAutoFocus = this.handleAutoFocus.bind(this);
    }

    delete() {
        const {deleteItem, id} = this.props;
        deleteItem(id);
    }

    update(key, value) {
        const {data, id, apiUpdateItem, updateItem} = this.props;
        const beforeUpdate = Object.assign( {}, data );
        
        Object.assign( data, { [key]: value } );

        if ( 'name' !== key ) {
            updateItem(data, beforeUpdate);
            apiUpdateItem(id, key);
        } else {
            updateItem(data);
        }
    }

    handleChange(event) {
        const target = event.target;
        const value = 'checkbox' === target.type ? target.checked : target.value;
        const name = target.name;

        this.update( name, value );

        if ( 'text' === target.type ) {
            this.setState({
                changedTextInput: true
            });
        }
    }

    handleFocus(event) {
        const {updateItem, data} = this.props;
        const beforeUpdate = Object.assign( {}, data );

        updateItem(data, beforeUpdate);

        this.setState( {
            inFocus: true,
            underlineShow: true 
        });
    }

    handleBlur() {
        const {id, apiUpdateItem} = this.props;
        this.setState({
            inFocus: false,
            underlineShow: false
        });

        if ( this.state.changedTextInput ) {
            apiUpdateItem(id, 'name');
            this.setState({
                changedTextInput: false
            });
        }
    }

    handleMouseOver() {
        this.setState({
            underlineShow: true
        });
    }

    handleMouseLeave() {
        if ( ! this.state.inFocus ) {
            this.setState({
                underlineShow: false
            });
        }
    }

    handleKeyPress(event) {
        if ( 'Enter' === event.key ) {
            event.target.blur();
        }
    }

    handleAutoFocus(input) {
        const {name, newlyCreated} = this.props.data;
        if ( ( !name || 0 === name.length ) && newlyCreated ) {
             input && input.focus();
        }
    }

    classNameOnDrag(isDragging) {
        return isDragging ? 'c-list__row c-list__row--dragged' : 'c-list__row';
    }

    render() {
        const {provided, snapshot} = this.props;
        const {id, name, is_complete} = this.props.data;
        const {underlineShow} = this.state;

        return (
            <Draggable key={id} draggableId={id}>
              {(provided, snapshot) => (
                <div>
                  <div className={this.classNameOnDrag(snapshot.isDragging)} ref={provided.innerRef} style={provided.draggableStyle}>
                    <div className="c-list__cell">
                        <FontIcon {...provided.dragHandleProps} className="material-icons c-list__drag-handle">drag_handle</FontIcon>
                        <Checkbox className="c-list__checkbox" name="is_complete" checked={is_complete} onCheck={this.handleChange} />
                    </div>
                    <div className="c-list__cell">
                      <TextField 
                        hintText="Task to do. Press <Enter> or click away to save" 
                        name="name" 
                        value={name}
                        fullWidth={true} 
                        underlineShow={underlineShow} 
                        onChange={this.handleChange} 
                        onFocus={this.handleFocus}
                        onMouseOver={this.handleMouseOver}
                        onBlur={this.handleBlur}
                        onMouseLeave={this.handleMouseLeave}
                        onKeyPress={this.handleKeyPress}
                        ref={this.handleAutoFocus}
                        />
                    </div>
                    <div className="c-list__cell">
                      <IconButton className="c-list__delete-btn" onClick={this.delete}>
                        <i className="material-icons">delete</i>
                      </IconButton>
                    </div>
                  </div>
                  {provided.placeholder}
                </div>
              )}
            </Draggable>
        );
    }    

};