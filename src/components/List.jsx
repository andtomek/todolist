import React from 'react'
import ReactDOM from 'react-dom'

import { reorder } from '../utils/utils';

import { DragDropContext, Droppable } from 'react-beautiful-dnd';

import TextField from 'material-ui/TextField';
import IconButton from 'material-ui/IconButton';

import ListHeader from './ListHeader';

export default class List extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            filter: 'all',
            searchQuery: ''
        }

        this.updateFilter = this.updateFilter.bind(this);
        this.updateSearchQuery = this.updateSearchQuery.bind(this);
        this.onDragEnd = this.onDragEnd.bind(this);
    }

    componentDidMount() {
        this.sortByCachedOrder();
    }

    componentDidUpdate(prevProps, prevState) {
        const {items, listName} = this.props;

        if ( items.length > 0 ) {
            const onlyIds = items.map( ({id}) => { return { id } } );
            localStorage.setItem( listName.parent, JSON.stringify(onlyIds) );
        }
    }

    updateFilter(event, filter) {
        this.setState( {
            filter
        });
    }

    updateSearchQuery( searchQuery ) {
        this.setState({
            searchQuery
        });
    }

    limitBySearch( items, searchQuery ) {
        if ( searchQuery.length > 0 ) {
            return items.filter( ( {name} ) => name.toLowerCase().indexOf( searchQuery.toLowerCase() ) > -1 || name.length === 0 );
        } else {
            return items;
        }
    }

    limitByFilter( items, filter ) {
        return items.filter( (item) => {   
            if ( 'all' === filter 
                 || ( 'complete' === filter && item.is_complete )
                 || ( 'incomplete' === filter && ! item.is_complete )
            ) {
                return true;
            }
        });
    }

    onDragEnd(result) {
        const { items, saveReordered } = this.props;

        if(! result.destination) {
           return; 
        }
        
        const reordered = reorder(
          items, 
          result.source.index, 
          result.destination.index
        );
        
        saveReordered(reordered);
    }

    getItemsCachedOrder(listName) {
        return JSON.parse( localStorage.getItem(listName) );
    }

    sortByCachedOrder() {
        const {items, listName, saveReordered} = this.props;

        const cachedOrder = this.getItemsCachedOrder(listName.parent) || [];

        const ordered = cachedOrder.map( (cachedItem) => {
            const itemIndex = items.findIndex( ({id}) =>  id === cachedItem.id );
            if ( itemIndex > -1 ) {
                return items[ itemIndex ];
            } 
        }).filter( (item) => item );

        const unordered = items.filter( (item) => {
            const orderedIndex = cachedOrder.findIndex( ({id}) =>  id === item.id );
            return orderedIndex === -1;
        });

        /* adding items without cached order (e.g. new added via API) */
        const all = ordered.concat( unordered ); 

        saveReordered(all);
    }

    add() {
        this.setState({
            filter: 'all',
            searchQuery: ''
        });
        this.props.addItem();
    }

    render() {
        const { filter, searchQuery} = this.state;
        const { items, childItem, deleteItem, updateItem, apiUpdateItem, addItem } = this.props;
        const ChildType = childItem;

        const filtered = this.limitByFilter( items, filter);
        const limitedBySearch = this.limitBySearch( filtered, searchQuery);

        return (
          <div className="c-list">
            <ListHeader 
              addItem={this.add.bind(this)} 
              search={this.updateSearchQuery} 
              setFilter={this.updateFilter} 
              filter={filter} 
              />

            <DragDropContext onDragEnd={this.onDragEnd}>
              <Droppable droppableId="droppable">        
                {(provided, snapshot) => (
                    <div ref={provided.innerRef}>
                        {limitedBySearch.map( (data, index) => (
                            <ChildType 
                              key={data.id} 
                              id={data.id}
                              data={data}
                              deleteItem={deleteItem} 
                              updateItem={updateItem} 
                              apiUpdateItem={apiUpdateItem}
                              
                              provided={provided}
                              snapshot={snapshot}
                              />
                        ))}
                    </div>
                )}
              </Droppable>
            </DragDropContext>        
                
            <div className="c-list__row">
              <div className="c-list__cell"></div>
              <div className="c-list__cell"></div>
              <div className="c-list__cell">
                <IconButton onClick={addItem}>
                  <i className="material-icons">add</i>
                </IconButton>
              </div>
            </div>
          </div>
        );
    }    

};