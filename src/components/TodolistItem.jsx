import React from 'react'
import ReactDOM from 'react-dom'

import { Link } from 'react-router-dom'

import TextField from 'material-ui/TextField';
import IconButton from 'material-ui/IconButton';
import FlatButton from 'material-ui/FlatButton';
import FontIcon from 'material-ui/FontIcon';

import { Draggable } from 'react-beautiful-dnd';

import ListItem from './ListItem';
import Navigate from './Navigate';

export default class TodolistItem extends ListItem {

    handleBlur(event) {
        const {data, apiUpdateItem} = this.props;

        this.update( 'name', event.target.value );
        apiUpdateItem( data.id, 'name' );
    }

    NameOrTextField(props) {
        const {name, id, todosCount, todosCompleted} = props;
        if ( !name || name.length === 0 ) {
            return <TextField 
                      hintText="New todolist name. Press <Enter> or click away to save" 
                      name="name" 
                      fullWidth={true} 
                      underlineShow={true}
                      onBlur={props.onBlur}
                      onKeyPress={props.onKeyPress}
                      ref={props.autoFocus}
                      />;
        } else {
            return (
                <Navigate to={{ pathname: "/todolist", state: { id, name, todosCount, todosCompleted } }} >
                    <FlatButton className="c-list__list-title" fullWidth={true} label={name} />
                </Navigate>
            )
        }
    }

    getTodolistStatus() {
        const {todos_count, todos_completed, is_complete} = this.props.data;

        if ( is_complete ) {
            return <FontIcon className="material-icons c-list__status">done</FontIcon>;
        } else if ( todos_completed > 0 ) {
            const percentage = Math.round( parseInt(todos_completed) / parseInt(todos_count) * 100 );
            return `${percentage}%`;
        } else {
            return '0%';
        }
    }

    render() {
        const {id, name, todos_count, todos_completed, is_complete} = this.props.data;
        const {provided, snapshot} = this.props;
        const NameOrTextField = this.NameOrTextField;
        const Status = this.getTodolistStatus();

        return (
            <Draggable key={id} draggableId={id}>
              {(provided, snapshot) => (
                <div>
                  <div className={this.classNameOnDrag(snapshot.isDragging)} ref={provided.innerRef} style={provided.draggableStyle}>
                    <div className="c-list__cell c-list__cell--status">
                      <FontIcon {...provided.dragHandleProps} className="material-icons c-list__drag-handle">drag_handle</FontIcon>
                      {Status}
                    </div>
                    <div className="c-list__cell">
                      <NameOrTextField 
                        id={id} 
                        name={name}
                        todosCount={todos_count}
                        todosCompleted={todos_completed}
                        onBlur={this.handleBlur} 
                        onKeyPress={this.handleKeyPress} 
                        autoFocus={this.handleAutoFocus}
                        />
                    </div>
                    <div className="c-list__cell">
                      <IconButton className="c-list__delete-btn" onClick={this.delete}>
                        <i className="material-icons">delete_forever</i>
                      </IconButton>
                    </div>
                  </div>
                  {provided.placeholder}
                </div>
              )}
            </Draggable>
        );
    }    

};