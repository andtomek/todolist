import React from 'react'
import ReactDOM from 'react-dom'

import List from '../components/List';

import Snackbar from 'material-ui/Snackbar';

import axios from 'axios';

import update from 'immutability-helper';

export default class ApiFetchedList extends React.Component {

    constructor(props) {
        super(props);

        this.errorMsgs = props.errorMsgs;

        this.state = {
            items:  [], 
            fetched: false,
            error: false,
            beforeLastUpdate: {}
        };

        this.addItem = this.addItem.bind(this);
        this.deleteItem = this.deleteItem.bind(this);
        this.updateItem = this.updateItem.bind(this);
        this.apiUpdateItem = this.apiUpdateItem.bind(this);
        this.clearError = this.clearError.bind(this);

        this.saveReordered = this.saveReordered.bind(this);
    }

    componentDidMount() {
        this.apiGetAllItems();
    }

    clearError() {
        this.setState({error: false});
    }

    saveReordered(reorderedItems) {
        this.setState({
            items: reorderedItems
        });
    }

    getReversedLastChange() {
        const {items, beforeLastUpdate} = this.state;

        return update( items, { [beforeLastUpdate.item]: { $set: beforeLastUpdate.data } } );
    }


    apiGetAllItems() {
        const {apiUrls, todolistId, listName} = this.props;

        axios.get( apiUrls.getAll )
          .then( ({data}) => {
            
            this.setState( {
                items: data, 
                fetched: true
            });
          })
          .catch( (error) => {
            console.error(error);
            this.setState({
                error: this.errorMsgs.getAllItems
            });
          });
    }

    apiDeleteItem(id) {
        const {apiUrls } = this.props;
        const {items} = this.state;

        return axios.delete( apiUrls.delete + id);
    }

    apiAddItem() {
        const { todolistId, apiUrls } = this.props;

        return axios.post( apiUrls.create, {
            name: "",
            todo_list: todolistId
        });
    }

    apiUpdateItem(id, key) {
        const itemIndex = this.findItem(id);
        const {items} = this.state;
        const theUpdate = { [key]: items[itemIndex][key] };

        return axios.put( this.props.apiUrls.update + id, theUpdate )
            .catch( (error) => {
                console.error(error);

                const reversed = this.getReversedLastChange();
                this.setState({
                    items: reversed,
                    error: this.errorMsgs.updateItem
                });
            });
    }

    

    findItem(id) {
        const {items} = this.state;

        return items.findIndex( (item) => {
            return item.id === id;
        } );
    }

    deleteItem(id) {
        const {items} = this.state;
        const toDelete = this.findItem(id);
        const {listName} = this.props;

        if ( -1 === toDelete ) {
            return;
        } 

        //deleting via API and from view
        this.apiDeleteItem(id)
          .then( () => {

            const afterDelete = update(items, { $splice: [[toDelete, 1]] });

            if ( listName.child ) {
                localStorage.removeItem( listName.child + id );
            }
            this.setState({ 
              items: afterDelete
            });
          })
          .catch( (error) => {
            console.error(error);
            this.setState({
                error: this.errorMsgs.deleteItem
            });
          });
    }

    addItem() {
        const {items} = this.state;

        this.apiAddItem()
          .then( (response) => {

            const newItem = Object.assign( response.data, {newlyCreated: true} );
            const afterAddition = update( items, { $push: [newItem] } );

            this.setState( {
              items: afterAddition
            });

          })
          .catch( (error) => {
            console.error(error);
            this.setState({
                error: this.errorMsgs.addItem
            });
          });
    }

    updateItem(details, beforeUpdate) {
        const {items, beforeLastUpdate} = this.state;
        const toUpdate = this.findItem(details.id);

        const afterUpdate = update( items, { [toUpdate]: { $set: details } });
        const justBeforeUpdate = beforeUpdate ? {item: toUpdate, data: beforeUpdate} : beforeLastUpdate;

        this.setState( {
            items: afterUpdate,
            beforeLastUpdate: justBeforeUpdate
        });

    }


    render() {
        const {id, name, items, fetched, error} = this.state;
        const {childComponent, map, listName} = this.props;
        let mapped;

        if ( map ) {
            mapped = items.map( map ); 
        }

        if ( items.length === 0 && !fetched ) {
            if (error) {
                return <Snackbar open={Boolean(error)} message={error} autoHideDuration={5000} onRequestClose={this.clearError} />;
            } else {
                return <div className="u-loading-status"></div>;
            }
        }

        return (
            <div>
                <Snackbar open={Boolean(error)} message={error} autoHideDuration={5000} onRequestClose={this.clearError}/>
                <List 
                  childItem={childComponent}
                  items={mapped || items} 
                  addItem={this.addItem} 
                  deleteItem={this.deleteItem}
                  updateItem={this.updateItem} 
                  apiUpdateItem={this.apiUpdateItem}
                  saveReordered={this.saveReordered}
                  listName={listName}
                  />
            </div>
        );
    }    

};