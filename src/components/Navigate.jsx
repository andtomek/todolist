import React from 'react'
import ReactDOM from 'react-dom'

import { Redirect } from 'react-router'

export default class Navigate extends React.Component {

  constructor() {
    super();
    this.state = {
      redirect: false
    }

    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    const {firstComplete} = this.props;

    if ( firstComplete ) {
      firstComplete()
        .then( () => {
          this.setState({
            redirect: true
          });
        })
        .catch( (error) => {
          console.error(error);
        });
    } else {
      this.setState({
        redirect: true
      });
    }
    
  }

  render() {
      const {to, className, children} = this.props;
      
      if ( this.state.redirect ) {
        return <Redirect to={to} push={true} />
      } else {
        return <span className={className} onClick={this.handleClick}>{children}</span> 
      }
  }    

};