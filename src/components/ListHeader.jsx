import React from 'react'
import ReactDOM from 'react-dom'

import TextField from 'material-ui/TextField';
import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';

import { lightBlue500 } from 'material-ui/styles/colors';

import { Link } from 'react-router-dom';

export default class ListHeader extends React.Component {

    searchBy(event) {
        const {setFilter, search} = this.props;
        const query = event.target.value;

        setFilter(null, 'all');

        search(query);
    }

    render() {
        const {setFilter, filter, addItem, addItemHref} = this.props;

        return (
            <div className="c-list__row c-list__row--head">
                <div className="c-list__cell">
                    <IconMenu
                      iconButtonElement={<IconButton className="c-list__filter-btn"><i className="material-icons">filter_list</i></IconButton>}
                      anchorOrigin={{horizontal: 'left', vertical: 'top'}}
                      targetOrigin={{horizontal: 'left', vertical: 'top'}}
                      onChange={setFilter}
                      value={filter}
                      selectedMenuItemStyle={{color: lightBlue500}}
                    >
                        <MenuItem value="all" primaryText="All" />
                        <MenuItem value="complete" primaryText="Complete" />
                        <MenuItem value="incomplete" primaryText="Incomplete" />
                    </IconMenu>

                </div>
                <div className="c-list__cell">
                    <TextField hintText="Search" onChange={this.searchBy.bind(this)} fullWidth={true} />
                </div>
                <div className="c-list__cell">
                    <IconButton onClick={addItem}>
                        <i className="material-icons">add</i>
                    </IconButton>
                </div>
            </div>
        );
    }    

};