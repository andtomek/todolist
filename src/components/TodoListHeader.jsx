import React from 'react'
import ReactDOM from 'react-dom'

import TextField from 'material-ui/TextField';
import IconButton from 'material-ui/IconButton';
import Snackbar from 'material-ui/Snackbar';

import axios from 'axios';

import Navigate from './Navigate';

export default class TodoListHeader extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            name: props.name,
            previousName: props.name,
            error: false
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleBlur = this.handleBlur.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
        this.handleKeyPress = this.handleKeyPress.bind(this);
    }

    handleChange(event) {
        const {value} = event.target;
        this.setState({
            name: value,
        });
    }

    handleBlur(event) {
        const {name, id} = this.props;
        const {value} = event.target;

        if ( value !== name ) {
            axios.put( `/api/todolists/${id}`, {
                name: value
            })
            .then( () => {
                //setting previous name in case of api update error, to restore it if needed
                this.setState({
                    previousName: value
                });
            })
            .catch( (error) => {
                console.error(error);
                this.setState( (oldState) => ({
                    error: "Error occurred when renaming the todolist. Please try again.",
                    name: oldState.previousName
                }));
            });
        }
    } 

    handleDelete() {
        const {id} = this.props;
        return axios.delete( `/api/todolists/${id}`)
            .then( () => {
                localStorage.removeItem( 'todolist' + id );
            })
            .catch( (error) => {
                this.setState({
                    error: "Error occurred when deleting the todolist. Please try again." 
                });
                throw new Error(error); //passing the error to prevent Navigate from redirecting
            });
    }

    handleKeyPress(event) {
        if ( 'Enter' === event.key ) {
            event.target.blur();
        }
    }

    render() {
        const {error, name} = this.state;
        return (
            <div className="c-todolist-header u-margin-bottom">
              <Snackbar open={Boolean(error)} message={error} autoHideDuration={5000} onRequestClose={(() => {this.setState({error: false})} )}/>
              <TextField 
                value={name} 
                hintText="Name your todolist" 
                onChange={this.handleChange}
                onBlur={this.handleBlur} 
                className="c-todolist-header__name"
                onKeyPress={this.handleKeyPress}
                />
              
              <Navigate to="/" firstComplete={this.handleDelete}>
                <IconButton className="c-todolist-header__delete-btn">
                  <i className="material-icons">delete_forever</i>
                </IconButton>
              </Navigate>
            </div>
        );
    }    

};