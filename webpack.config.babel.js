import webpack from 'webpack'
import path from 'path'
import merge from 'webpack-merge'
import autoprefixer from 'autoprefixer'
import globImporter from 'node-sass-glob-importer';
import ExtractTextPlugin from 'extract-text-webpack-plugin';

const common = {
  entry: [ 'babel-polyfill', path.resolve(__dirname, './src') ],
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.js'
  },
  module: {
    rules: [
      { test: /\.jsx?$/, use: 'babel-loader' },
      { test: /\.exec\.js$/, use: [ 'script-loader' ] },
      {
        test: /\.s?css$/,
        use: [{
          loader: 'style-loader'
        }, {
          loader: 'css-loader'
        }, {
          loader: 'postcss-loader',
          options: {
            plugins: [ autoprefixer ]
          }
        }, {
          loader: 'sass-loader',
          options: {
            includePaths: [
              path.resolve(__dirname, './node_modules')
            ],
            importer: globImporter()
          }
        }]
      }

    ]
  },
  resolve: {
    extensions: ['.js', '.jsx', '.scss', '.css']
  }
}

const dev = {
  devServer: {
    historyApiFallback:{
      index: 'index.html'
    },
    hot: true,
    contentBase: path.resolve(__dirname, 'src/public_static'),
    publicPath: '/',
    proxy: {
    '/api': {
        target: 'http://localhost:3000/',
        secure: false,
        changeOrigin: true,
        pathRewrite: function (path, req) {
          const newPath = path.replace('/api', '');
          return newPath;
        }
      }
    }
  },
  devtool: 'source-map'
}

const prod = {
  plugins: [
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify('production')
    }),
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false,
      },
      sourceMap: false
    })
  ]
}

const config = (env) => {
  switch (env) {
    case 'build':
      return merge(common, prod)
    case 'start':
      return merge(common, dev)
    default:
      return common
  }
}

export default config(process.env.npm_lifecycle_event)
